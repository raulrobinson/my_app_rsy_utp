import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
<<<<<<< HEAD
import 'package:shared_preferences/shared_preferences.dart';

import '../pages/take_photo.dart';

class PhotoAvatarWidget extends StatefulWidget {
  Function? action;
  String? photo;
  PhotoAvatarWidget({super.key});
=======

import '../../model/entity/sale.dart';
import '../pages/take_photo.dart';

class PhotoAvatarWidget extends StatefulWidget {
  final SaleEntity sale;
  const PhotoAvatarWidget({super.key, required this.sale});
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9

  @override
  State<PhotoAvatarWidget> createState() => _PhotoAvatarWidgetState();
}

class _PhotoAvatarWidgetState extends State<PhotoAvatarWidget> {
<<<<<<< HEAD
  final _pref = SharedPreferences.getInstance();

  @override
  void initState() {
    super.initState();

    _pref.then((pref) {
      setState(() {
        widget.photo = pref.getString("photo");
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget icono;
    if (widget.photo == null) {
=======
  @override
  Widget build(BuildContext context) {
    Widget icono;
    if (widget.sale.photo == null) {
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
      icono = IconButton(
        icon: const Icon(Icons.camera_alt),
        onPressed: () async {
          var nav = Navigator.of(context);

          final cameras = await availableCameras();
          final camera = cameras.first;

          var imagePath = await nav.push<String>(
            MaterialPageRoute(
              builder: (context) => TakePhotoPage(camera: camera),
            ),
          );

          if (imagePath != null && imagePath.isNotEmpty) {
            setState(() {
<<<<<<< HEAD
              widget.photo = imagePath;
              if (widget.action != null) {
                widget.action!(imagePath);
              }
=======
              widget.sale.photo = imagePath;
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
            });
          }
        },
      );
    } else {
<<<<<<< HEAD
      ImageProvider provider;
      if (widget.photo!.startsWith("http")) {
        provider = NetworkImage(widget.photo!);
      } else {
        provider = FileImage(File(widget.photo!));
      }
      icono = CircleAvatar(
        radius: 30,
        backgroundImage: provider,
=======
      icono = CircleAvatar(
        radius: 30,
        backgroundImage: FileImage(File(widget.sale.photo!)),
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
      );
    }

    return icono;
  }
}
