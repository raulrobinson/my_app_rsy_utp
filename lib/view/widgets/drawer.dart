import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
<<<<<<< HEAD
import 'package:ventas_31269/controller/login.dart';
import 'package:ventas_31269/view/pages/login.dart';
import 'package:ventas_31269/view/widgets/photo_avatar.dart';
import '../pages/cash_close.dart';
=======
import '../../controller/login.dart';
import '../pages/cash_close.dart';
import '../pages/login.dart';
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
import '../pages/payments.dart';

class DrawerWidget extends StatefulWidget {
  const DrawerWidget({super.key});

  @override
  State<DrawerWidget> createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  final _pref = SharedPreferences.getInstance();
  final _loginController = LoginController();
<<<<<<< HEAD
  final _avatar = PhotoAvatarWidget();
  String _uid = "";
=======
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
  String _name = "";
  String _email = "";
  bool _isAdmin = false;

  @override
  void initState() {
    super.initState();

<<<<<<< HEAD
    _avatar.action = (photo) async {
      // Actualice la foto en la base de datos
      photo = await _loginController.updatePhoto(_uid, photo);

      var pref = await _pref;
      pref.setString("photo", photo);
    };

    _pref.then((pref) {
      setState(() {
        _uid = pref.getString("uid") ?? "";
=======
    _pref.then((pref) {
      setState(() {
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
        _name = pref.getString("name") ?? "N/A";
        _email = pref.getString("email") ?? "N/A";
        _isAdmin = pref.getBool("admin") ?? false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
<<<<<<< HEAD
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
=======
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
<<<<<<< HEAD
            decoration: BoxDecoration(
              color: Theme.of(context).appBarTheme.backgroundColor,
=======
            decoration: const BoxDecoration(
              color: Colors.blue,
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
            ),
            child: _header(),
          ),
          ListTile(
            leading: const Icon(Icons.payment),
            title: const Text('Devices'),
            onTap: () {
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(builder: (context) => const PaymentsPage()),
              );
            },
          ),
          ListTile(
            leading: const Icon(Icons.shopping_bag),
            title: const Text('Equipment'),
            onTap: () {},
          ),
          ListTile(
            leading: const Icon(Icons.supervisor_account),
            title: const Text('Groups'),
            onTap: () {},
          ),
          if (_isAdmin)
            ListTile(
              leading: const Icon(Icons.medication_rounded),
              title: const Text('Groups'),
              onTap: () {},
            ),
          ListTile(
            leading: const Icon(Icons.money),
            title: const Text('Close Connection'),
            onTap: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const CashClosePage(),
                  ));
            },
          ),
          ListTile(
            leading: const Icon(Icons.exit_to_app),
            title: const Text('Exit'),
            onTap: () async {
              var nav = Navigator.of(context);

              // Cerrar sesion en Auth de Firebase
              _loginController.logout();

              // Limpiar las preferences
              var pref = await _pref;
              pref.remove("uid");
              pref.remove("email");
              pref.remove("name");
              pref.remove("admin");
<<<<<<< HEAD
              pref.remove("photo");
=======
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9

              // Volver a pagina de login
              nav.pushReplacement(
                MaterialPageRoute(builder: (context) => LoginPage()),
              );
            },
          ),
        ],
      ),
    );
  }

  Widget _header() {
<<<<<<< HEAD
    // Consultar los datos de la cabecera
    return Row(
      children: [
        _avatar,
=======
    // TODO: Consultar los datos de la cabecera
    const image = Icon(Icons.manage_accounts);

    return Row(
      children: [
        const CircleAvatar(
          radius: 30,
          child: image,
        ),
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
        const SizedBox(width: 8),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                _name,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                ),
              ),
              const SizedBox(height: 8),
              Text(
                _email,
                style: const TextStyle(
                  color: Colors.white,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
