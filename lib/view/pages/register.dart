import 'package:flutter/material.dart';
<<<<<<< HEAD
import 'package:ventas_31269/controller/login.dart';

import '../../controller/request/register.dart';

class RegisterPage extends StatelessWidget {
  late final RegisterRequest _data;
  late final LoginController _controller;
=======

import '../../controller/login.dart';
import '../../controller/request/register.dart';

class RegisterPage extends StatelessWidget {
  late RegisterRequest _data;
  late LoginController _controller;
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9

  RegisterPage({super.key}) {
    _data = RegisterRequest();
    _controller = LoginController();
  }

  @override
  Widget build(BuildContext context) {
    var formKey = GlobalKey<FormState>();

    return Scaffold(
      appBar: AppBar(
        title: const Text("Register"),
      ),
      body: Form(
        key: formKey,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                _basicWidget(
                  "Name",
                  validarCampoObligatorio,
                  (newValue) {
                    _data.name = newValue!;
                  },
                ),
                _basicWidget(
                  "Address",
                  validarCampoObligatorio,
                  (newValue) {
                    _data.address = newValue!;
                  },
                ),
                _basicWidget(
                  "Email",
                  validarCampoObligatorio,
                  (newValue) {
                    _data.email = newValue!;
                  },
                ),
                _basicWidget(
                  "Phone",
                  (value) => null,
                  (newValue) {
                    _data.phone = newValue;
                  },
                ),
                _basicWidget(
                  "Password",
                  validarCampoObligatorio,
                  (newValue) {
                    _data.password = newValue!;
                  },
                  isPassword: true,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (formKey.currentState!.validate()) {
                      formKey.currentState!.save();

<<<<<<< HEAD
                      final nav = Navigator.of(context);
                      final messager = ScaffoldMessenger.of(context);
                      try {
                        await _controller.registerNewUser(_data);

                        messager.showSnackBar(
=======
                      try {
                        await _controller.registerNewUser(_data);

                        ScaffoldMessenger.of(context).showSnackBar(
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
                          const SnackBar(
                              content:
                                  Text("User register success")),
                        );

<<<<<<< HEAD
                        nav.pop();
                      } catch (error) {
                        messager.showSnackBar(
=======
                        Navigator.pop(context);
                      } catch (error) {
                        ScaffoldMessenger.of(context).showSnackBar(
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
                          SnackBar(
                            content: Text(error.toString()),
                          ),
                        );
                      }
                    }
                  },
                  child: const Text("Save"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  String? validarCampoObligatorio(String? value) {
    if (value == null || value.isEmpty) {
      return "El campo es obligatorio";
    }
    return null;
  }

  Widget _basicWidget(String title, FormFieldValidator<String?> validate,
      FormFieldSetter<String?> save,
      {bool isPassword = false}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: TextFormField(
        maxLength: 50,
        obscureText: isPassword,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          border: const OutlineInputBorder(),
          labelText: title,
        ),
        validator: validate,
        onSaved: save,
      ),
    );
  }
}
