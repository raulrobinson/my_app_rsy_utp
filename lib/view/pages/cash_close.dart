import 'package:flutter/material.dart';

import '../widgets/drawer.dart';

class CashClosePage extends StatelessWidget {
  const CashClosePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Inventories"),
      ),
      drawer: const DrawerWidget(),
    );
  }
}
