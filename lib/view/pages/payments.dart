import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
<<<<<<< HEAD
import 'package:url_launcher/url_launcher.dart';
import 'package:ventas_31269/controller/sales.dart';
import 'package:ventas_31269/model/entity/sale.dart';
=======
import '../../controller/sales.dart';
import '../../model/entity/sale.dart';
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
import 'new_sale.dart';
import '../widgets/drawer.dart';

class PaymentsPage extends StatefulWidget {
  const PaymentsPage({super.key});

  @override
  State<PaymentsPage> createState() => _PaymentsPageState();
}

class _PaymentsPageState extends State<PaymentsPage> {
  List<SaleEntity> _lista = [];
  final _pref = SharedPreferences.getInstance();
  final _saleController = SaleController();

  @override
  void initState() {
    super.initState();
    _listarCobros();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Conexiones"),
        ),
        drawer: const DrawerWidget(),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "Equipos",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Expanded(
                child: ListView.builder(
                    itemCount: _lista.length,
                    itemBuilder: (context, index) {
                      final sale = _lista[index];

                      final CircleAvatar avatar;
                      if (sale.photo != null) {
                        avatar = CircleAvatar(
                          radius: 25,
<<<<<<< HEAD
                          backgroundImage: NetworkImage(sale.photo!),
=======
                          backgroundImage: FileImage(File(sale.photo!)),
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
                        );
                      } else {
                        avatar = const CircleAvatar(
                          radius: 25,
                          child: Icon(Icons.account_circle),
                        );
                      }

                      return ListTile(
                        leading: avatar,
                        title: Text(sale.clientName!),
                        subtitle: Text(sale.address!),
<<<<<<< HEAD
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
=======
                        trailing:
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
                            ((sale.phone != null && sale.phone!.isNotEmpty)
                                ? IconButton(
                                    icon: const Icon(Icons.phone),
                                    onPressed: () {
<<<<<<< HEAD
                                      // Realizar la llamada Telefonica
                                      var url = Uri(
                                        scheme: "tel",
                                        path: sale.phone,
                                      );
                                      // Abrir con Whatsapp
                                      // var url = Uri(
                                      //   scheme: "https",
                                      //   path:
                                      //       "wa.me/${sale.phone}/?text=Hola+Mundo",
                                      // );
                                      launchUrl(url);
                                    },
                                  )
                                : const Text("")),
                            (sale.lat != null
                                ? IconButton(
                                    icon: const Icon(Icons.pin_drop_rounded),
                                    onPressed: () {
                                      // mostrar ruta hasta el lugar
                                      // var url = Uri(
                                      //   scheme: "google.navigation",
                                      //   path:
                                      //       "q=${sale.lat},${sale.lng}&mode=d",
                                      // );
                                      var url = Uri(
                                        scheme: "geo",
                                        path: "${sale.lat},${sale.lng}",
                                      );
                                      launchUrl(url);
                                    },
                                  )
                                : const Text(""))
                          ],
                        ),
=======
                                      // TODO Realizar la llamada Telefonica
                                    },
                                  )
                                : const Text("")),
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
                        onTap: () {
                          // TODO: Ir a la ventana detalle del cobro
                        },
                      );
                    }),
              )
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.add_shopping_cart),
          onPressed: () async {
            await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => NewSalePage(),
              ),
            );

            if (!mounted) return;

            _listarCobros();
          },
        ),
      ),
    );
  }

  void _listarCobros() {
    _pref.then((pref) {
      var id = pref.getString("uid") ?? "";
      _saleController.listAll(id).then((value) {
        setState(() {
          _lista = value;
        });
      });
    });
  }
}
