import 'package:flutter/material.dart';
<<<<<<< HEAD
import 'package:location/location.dart';
=======
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
import 'package:shared_preferences/shared_preferences.dart';

import '../../controller/sales.dart';
import '../../model/entity/sale.dart';
import '../widgets/photo_avatar.dart';

class NewSalePage extends StatelessWidget {
  final _pref = SharedPreferences.getInstance();
  late final SaleEntity _sale;
  late final SaleController _controller;
<<<<<<< HEAD
  late final PhotoAvatarWidget _avatar;
=======
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9

  final _txtMonto = TextEditingController(text: "0");
  final _txtCuotas = TextEditingController(text: "1");
  final _txtValor = TextEditingController(text: "0");

  NewSalePage({super.key}) {
    _sale = SaleEntity();
    _controller = SaleController();
    _pref.then((pref) {
      _sale.user = pref.getString("uid");
    });
<<<<<<< HEAD
    _avatar = PhotoAvatarWidget();
=======
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Register"),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "New Equipment",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 16),
              _formulario(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _formulario(BuildContext context) {
    final formKey = GlobalKey<FormState>();

    return Form(
      key: formKey,
      child: Column(
        children: [
          _campoCliente(),
          const SizedBox(height: 8),
          _campoDireccion(),
          const SizedBox(height: 8),
          _campoTelefono(),
          const SizedBox(height: 8),
          _campoMonto(context),
          const SizedBox(height: 8),
          Row(
            children: [
              _numeroCuotas(context),
              const SizedBox(width: 8),
              _periodicidad(context),
            ],
          ),
          const SizedBox(height: 8),
          _valorCuota(),
          const SizedBox(height: 8),
          _botonGuardar(context, formKey),
        ],
      ),
    );
  }

  Widget _campoCliente() {
    return TextFormField(
      maxLength: 100,
      autofocus: true,
      keyboardType: TextInputType.name,
      decoration: InputDecoration(
<<<<<<< HEAD
        icon: _avatar,
=======
        icon: PhotoAvatarWidget(
          sale: _sale,
        ),
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
        border: const OutlineInputBorder(),
        labelText: 'Device Name',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Field error";
        }
        return null;
      },
      onSaved: (value) {
        _sale.clientName = value!;
      },
    );
  }

  Widget _campoDireccion() {
    return TextFormField(
      keyboardType: TextInputType.streetAddress,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Geolocation',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Error";
        }
        return null;
      },
      onSaved: (value) {
        _sale.address = value!;
      },
    );
  }

  Widget _campoTelefono() {
    return TextFormField(
      keyboardType: TextInputType.phone,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Access Code',
      ),
      onSaved: (value) {
        _sale.phone = value!;
      },
    );
  }

  Widget _campoMonto(BuildContext context) {
    return TextFormField(
      controller: _txtMonto,
      textAlign: TextAlign.right,
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Token',
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Error";
        }
        if (int.parse(value) < 1) {
          return "Error value";
        }
        return null;
      },
      onChanged: (value) {
        if (value.isNotEmpty) {
          _calcularValorCuota(context);
        }
      },
      onSaved: (value) {
        _sale.ammount = int.tryParse(value!);
      },
    );
  }

  Widget _numeroCuotas(BuildContext context) {
    return Expanded(
      child: TextFormField(
        controller: _txtCuotas,
        textAlign: TextAlign.right,
        keyboardType: TextInputType.number,
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Serial Number',
        ),
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "Not valid";
          }
          if (int.parse(value) < 1) {
            return "Value with error";
          }
          return null;
        },
        onChanged: (value) {
          if (value.isNotEmpty) {
            _calcularValorCuota(context);
          }
        },
        onSaved: (value) {
          _sale.parts = int.tryParse(value!);
        },
      ),
    );
  }

  Widget _periodicidad(BuildContext context) {
    var opciones = <String>["Diary", "Week", "Month", "Anual"];
    var valor = opciones[1];
    _sale.periodicity = opciones[1];

    return Expanded(
      child: DropdownButtonFormField(
        value: valor,
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
          labelText: 'Schedule',
        ),
        items: opciones
            .map<DropdownMenuItem<String>>(
                (String value) => DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    ))
            .toList(),
        onChanged: (value) {
          _sale.periodicity = value;

          _calcularValorCuota(context);
        },
      ),
    );
  }

  Widget _valorCuota() {
    return TextFormField(
      enabled: false,
      textAlign: TextAlign.right,
      controller: _txtValor,
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        labelText: 'Code value',
      ),
      onSaved: (value) {
        _sale.value = double.tryParse(value!);
      },
    );
  }

  Widget _botonGuardar(BuildContext context, GlobalKey<FormState> formKey) {
    return ElevatedButton(
      child: const Text("Save"),
      onPressed: () async {
        if (formKey.currentState!.validate()) {
<<<<<<< HEAD
          formKey.currentState!.save();
          _sale.photo = _avatar.photo;
=======
          print("antes: $_sale");
          formKey.currentState!.save();
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
          // Guardar los datos en la BD
          try {
            final mess = ScaffoldMessenger.of(context);
            final nav = Navigator.of(context);

<<<<<<< HEAD
            // Captura la informacion de la ubicacion del usuario
            await _getGPSLocation();

=======
            print("despues: $_sale");
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
            await _controller.save(_sale);

            mess.showSnackBar(
              const SnackBar(
                content: Text("Register Success"),
              ),
            );

            // Volver a la pantalla anterior
            nav.pop();
          } catch (e) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text("Error: $e"),
              ),
            );
          }
        }
      },
    );
  }

  void _calcularValorCuota(BuildContext context) {
    var amount = int.parse(_txtMonto.text);
    var parts = int.parse(_txtCuotas.text);
    var periodicity = _sale.periodicity ?? "";

    _controller.calculateValue(amount, parts, periodicity).then((value) {
      _txtValor.text = value.toString();
    }).onError((error, stackTrace) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text("$error")),
      );
    });
  }
<<<<<<< HEAD

  Future<void> _getGPSLocation() async {
    final location = Location();

    var serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) {
        return;
      }
    }

    var permission = await location.hasPermission();
    if (permission == PermissionStatus.denied) {
      permission = await location.requestPermission();
      if (permission != PermissionStatus.granted) {
        return;
      }
    }

    final locationData = await location.getLocation();
    _sale.lat = locationData.latitude;
    _sale.lng = locationData.longitude;
  }
=======
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
}
