<<<<<<< HEAD
import 'package:ventas_31269/model/entity/user.dart';
import 'package:ventas_31269/model/repository/fb_storage.dart';

=======
import '../model/entity/user.dart';
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
import '../model/repository/fb_auth.dart';
import '../model/repository/user.dart';
import 'request/login.dart';
import 'request/register.dart';
import 'response/userinfo.dart';

class LoginController {
  late final UserRepository _userRepository;
  late final FirebaseAuthenticationRepository _authRepository;
<<<<<<< HEAD
  late final FirebaseStorageRepository _storageRepository;
=======
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9

  LoginController() {
    _userRepository = UserRepository();
    _authRepository = FirebaseAuthenticationRepository();
<<<<<<< HEAD
    _storageRepository = FirebaseStorageRepository();
=======
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
  }

  Future<UserInfoResponse> validateEmailPassword(LoginRequest request) async {
    await _authRepository.signInEmailPassword(request.email, request.password);

    // Consultar el usuario que tenga el correo dado
    var user = await _userRepository.findByEmail(request.email);

    return UserInfoResponse(
      id: user.id,
      email: user.email,
      name: user.name,
      isAdmin: user.isAdmin,
<<<<<<< HEAD
      photo: user.photo,
=======
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
    );
  }

  Future<void> registerNewUser(RegisterRequest request,
      {bool adminUser = false}) async {
    // Consulto si el usuario ya existe
    try {
      await _userRepository.findByEmail(request.email);
      return Future.error("Ya existe un usuario con este correo electrónico");
    } catch (e) {
      // No existe el correo en la base de datos

      // Crear correo/clave en Firebase Authentication
      await _authRepository.createEmailPasswordAccount(
          request.email, request.password);

      // Agregar informacion adicional en base de datos
      _userRepository.save(UserEntity(
          email: request.email,
          name: request.name,
          address: request.address,
          phone: request.phone,
          isAdmin: adminUser));
    }
  }

  Future<void> logout() async {
    await _authRepository.signOut();
  }
<<<<<<< HEAD

  Future<String> updatePhoto(String uid, String photo) async {
    photo = await _storageRepository.loadFile(photo, "user/photo");
    await _userRepository.updatePhoto(uid, photo);
    return photo;
  }
=======
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
}
