import 'package:cloud_firestore/cloud_firestore.dart';

class UserEntity {
  late String? id;
  late String? email;
  late String? name;
  late String? address;
  late String? phone;
<<<<<<< HEAD
  late String? photo;
  late bool? isAdmin;

  UserEntity(
      {this.email,
      this.name,
      this.address,
      this.phone,
      this.photo,
      this.isAdmin});
=======
  late bool? isAdmin;

  UserEntity({this.email, this.name, this.address, this.phone, this.isAdmin});
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9

  factory UserEntity.fromFirestore(
      DocumentSnapshot<Map<String, dynamic>> snapshot,
      SnapshotOptions? options) {
    var data = snapshot.data();

    return UserEntity(
<<<<<<< HEAD
      email: data?["email"],
      name: data?["name"],
      address: data?["address"],
      phone: data?["phone"],
      photo: data?["photo"],
      isAdmin: data?["isAdmin"],
    );
=======
        email: data?["email"],
        name: data?["name"],
        address: data?["address"],
        phone: data?["phone"],
        isAdmin: data?["isAdmin"]);
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (email != null && email!.isNotEmpty) "email": email,
      if (name != null && name!.isNotEmpty) "name": name,
      if (address != null && address!.isNotEmpty) "address": address,
      if (phone != null && phone!.isNotEmpty) "phone": phone,
<<<<<<< HEAD
      if (photo != null && photo!.isNotEmpty) "photo": photo,
=======
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
      "isAdmin": isAdmin ?? false
    };
  }

  @override
  String toString() {
    return "UserEntity {$email, $name, $address, $phone, $isAdmin}";
  }
}
