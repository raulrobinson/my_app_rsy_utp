import 'package:cloud_firestore/cloud_firestore.dart';

class SaleEntity {
  late String? id;
  late String? user;
  late String? photo;
  late String? clientName;
  late String? address;
  late String? phone;
  late int? ammount;
  late int? parts;
  late String? periodicity;
  late double? value;

<<<<<<< HEAD
  late double? lat;
  late double? lng;

=======
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
  SaleEntity(
      {this.user,
      this.photo,
      this.clientName,
      this.address,
      this.phone,
      this.ammount,
      this.parts,
      this.periodicity,
      this.value,
<<<<<<< HEAD
      this.id,
      this.lat,
      this.lng});
=======
      this.id});
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9

  factory SaleEntity.fromFirestore(
      DocumentSnapshot<Map<String, dynamic>> snapshot,
      SnapshotOptions? options) {
    var data = snapshot.data();

    return SaleEntity(
<<<<<<< HEAD
      id: snapshot.id,
      user: data?["user"],
      photo: data?["photo"],
      clientName: data?["clientName"],
      address: data?["address"],
      phone: data?["phone"],
      ammount: data?["ammount"],
      parts: data?["parts"],
      periodicity: data?["periodicity"],
      value: data?["value"],
      lat: data?["lat"],
      lng: data?["lng"],
    );
=======
        id: snapshot.id,
        user: data?["user"],
        photo: data?["photo"],
        clientName: data?["clientName"],
        address: data?["address"],
        phone: data?["phone"],
        ammount: data?["ammount"],
        parts: data?["parts"],
        periodicity: data?["periodicity"],
        value: data?["value"]);
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (user != null && user!.isNotEmpty) "user": user,
      if (clientName != null && clientName!.isNotEmpty)
        "clientName": clientName,
      if (photo != null) "photo": photo,
      if (address != null && address!.isNotEmpty) "address": address,
      if (phone != null) "phone": phone,
      if (ammount != null) "ammount": ammount,
      if (parts != null) "parts": parts,
      if (periodicity != null && periodicity!.isNotEmpty)
        "periodicity": periodicity,
<<<<<<< HEAD
      if (value != null) "value": value,
      if (lat != null) "lat": lat,
      if (lng != null) "lng": lng
=======
      if (value != null) "value": value
>>>>>>> a486b5a710eb54d062afd20805429e8eee7b36f9
    };
  }

  @override
  String toString() {
    return "Sale(user: $user, clientName: $clientName, photo: $photo, address: $address, phone: $phone)";
  }
}
